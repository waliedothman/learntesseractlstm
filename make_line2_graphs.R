library(data.table)
library(stringr)
library(ggplot2)

numextract <- function(string){
  str_extract(string, "\\-*\\d+\\.*\\d*")
}

Numextract <- function(string){
  unlist(regmatches(string,gregexpr("[[:digit:]]+\\.*[[:digit:]]*",string)))
}



NN_specifications <- c(
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx128_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx192_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx256_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx384_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx512_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx128_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx192_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx384_O1c1",
  "1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx512_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx128_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx192_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx256_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx384_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys48_Lfx96_Lrx96_Lfx512_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx128_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx192_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx384_O1c1",
  "1.48.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx512_O1c1"
)

pdf(file = "Performance_MRZ_line2.pdf",height = 10, width = 15)
for (NN_spec in NN_specifications){
  
  backlog_file <- 
    data.table(
      output = readLines(paste0("./test_line2/output_num_GRID_",NN_spec,"/basetrain.log"))
    )
  
  backlog_file <- 
    backlog_file[,{
      is_iter_number = startsWith(as.character(output),"Iteration ")
      iter_number = ifelse(is_iter_number,as.numeric(numextract(as.character(output))),-1)
      is_iter_info = startsWith(as.character(output),"Mean rms=")
      list(is_iter_info = is_iter_info,
           is_iter_number = is_iter_number,
           iter_number = iter_number)
    },by = list(output = output)]
  
  backlog_file[,iter_number_fill := iter_number[1], .(cumsum(iter_number != -1))]
  
  rec_info <- backlog_file[is_iter_info == TRUE]
  
  rec_info <-
    rec_info[,{
      mean_rms = as.numeric(Numextract(output)[1])
      delta = as.numeric(Numextract(output)[2])
      character_error = as.numeric(Numextract(output)[3])
      word_error = as.numeric(Numextract(output)[4])
      skip_ratio = as.numeric(Numextract(output)[5])
      list(mean_rms = mean_rms,
           delta = delta,
           character_error = character_error,
           word_error = word_error,
           skip_ratio = skip_ratio)
    },
    by = list(output,
              iter_number = iter_number_fill)]
  
  
  print(  
    ggplot(data=rec_info) +
      geom_line(aes(x = iter_number, y = character_error, color = "Character error, %")) +
      geom_line(aes(x = iter_number, y = word_error, color = "Word error, %")) +
      geom_line(aes(x = iter_number, y = mean_rms, color = "Mean RMS, %")) +
      geom_line(aes(x = iter_number, y = delta, color = "Delta, %")) +
      geom_line(aes(x = iter_number, y = skip_ratio, color = "Skip ratio, %")) +
      theme(axis.text.x = element_text(size = 15), 
            axis.title.x = element_text(size = 20),   
            axis.text.y = element_text(size = 15),
            axis.title.y = element_text(size = 20),
            legend.title = element_text(size = 15),
            legend.text = element_text(size = 15)) +
      scale_color_discrete(name = "Characteristics, %") +
      scale_x_continuous(limits = c(-10, 10000),
                         breaks = c(0,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000)) +
      ylim(-1,110) +
      xlab("Number of iterations") + 
      ylab("Percentage, %") + 
      labs(title=paste0("NN spec:",NN_spec), size = 20)
  )  
  
}
dev.off()

