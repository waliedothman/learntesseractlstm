#!/bin/bash

# examples for reference https://github.com/Shreeshrii/tessdata_shreetest
# tesseract 4.1 would allow to whitelist chars using the lstm engine, tesseract 4 does not

# NIKTRAIN=NIKtrain
# NIKEVAL=NIKeval
ETKPTRAIN=etkptrain
ETKPEVAL=etkpeval

# mkdir ${NIKTRAIN}
# mkdir ${NIKEVAL}
mkdir ${ETKPTRAIN}
mkdir ${ETKPEVAL}


# generate synthetic data, if only for a test run, you might want to change the number of generated strings to a single digit
python ./machinereadablepassport/generateidcardstrings.py

# use --training_text to feed it to tesstrain.sh
# tesstrain.sh --fonts_dir ./fonts --lang eng --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "OCR A Extended" --output_dir ${NIKTRAIN} --training_text id_NIK_train.txt --distort_image 
# tesstrain.sh --fonts_dir ./fonts --lang eng --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "OCR A Extended" --output_dir ${NIKEVAL} --training_text id_NIK_test.txt --distort_image 
tesstrain.sh --fonts_dir ./fonts --lang ind --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "ektp" --output_dir ${ETKPTRAIN} --training_text id_etkp_train.txt --distort_image 
tesstrain.sh --fonts_dir ./fonts --lang ind --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "ektp" --output_dir ${ETKPEVAL} --training_text id_etkp_test.txt --distort_image 

# exit 0

# extract a starting point to start lstm training from
# combine_tessdata -e ./tessdata_best/eng.traineddata trainfrom_NIK.lstm
combine_tessdata -e ./tessdata_best/ind.traineddata trainfrom_etkp.lstm
# this is going to take a while, test different parameters to see how they converge
# ./train_different_network_parameters_NIK.sh
./train_different_network_parameters_etkp.sh
# different network parameters didn't make a significant difference

# create graphs and store them in a pdf
# Rscript make_NIK_graphs.R
Rscript make_etkp_graphs.R

# create trainingdata file for tesseract to use
# lstmtraining --stop_training --continue_from test_NIK/output_num_GRID_1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1/lstm-out/base_checkpoint --traineddata NIKtrain/eng/eng.traineddata --model_output ./NIK.traineddata
lstmtraining --stop_training --continue_from test_etkp/output_num_GRID_1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1/lstm-out/base_checkpoint --traineddata etkptrain/ind/ind.traineddata --model_output ./etkp.traineddata

# test individual images like this
# tesseract ./passportimages/1.png ./test --tessdata-dir ./ --oem 1 --psm 6 -l passportline1+passportline2
# cat test.txt