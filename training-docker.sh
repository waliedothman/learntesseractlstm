#!/bin/bash

# examples for reference https://github.com/Shreeshrii/tessdata_shreetest
# tesseract 4.1 would allow to whitelist chars using the lstm engine, tesseract 4 does not

LINE1TRAIN=line1train
LINE2TRAIN=line2train
LINE1EVAL=line1eval
LINE2EVAL=line2eval

mkdir ${LINE1TRAIN}
mkdir ${LINE1EVAL}
mkdir ${LINE2TRAIN}
mkdir ${LINE2EVAL}


# generate synthetic data, if only for a test run, you might want to change the number of generated strings to a single digit
python3 ./machinereadablepassport/generatepassportstrings.py

# use --training_text to feed it to tesstrain.sh
../tesseract/src/training/tesstrain.sh --fonts_dir /usr/share/fonts/opentype/ocr-b/ --lang eng --linedata_only --noextract_font_properties --langdata_dir ../langdata --tessdata_dir ../tessdata_best --fontlist "OCRB" --output_dir ${LINE1TRAIN} --training_text passportline1strings_train.txt
../tesseract/src/training/tesstrain.sh --fonts_dir /usr/share/fonts/opentype/ocr-b/ --lang eng --linedata_only --noextract_font_properties --langdata_dir ../langdata --tessdata_dir ../tessdata_best --fontlist "OCRB" --output_dir ${LINE1EVAL} --training_text passportline1strings_eval.txt
../tesseract/src/training/tesstrain.sh --fonts_dir /usr/share/fonts/opentype/ocr-b/ --lang eng --linedata_only --noextract_font_properties --langdata_dir ../langdata --tessdata_dir ../tessdata_best --fontlist "OCRB" --output_dir ${LINE2TRAIN} --training_text passportline2strings_train.txt
../tesseract/src/training/tesstrain.sh --fonts_dir /usr/share/fonts/opentype/ocr-b/ --lang eng --linedata_only --noextract_font_properties --langdata_dir ../langdata --tessdata_dir ../tessdata_best --fontlist "OCRB" --output_dir ${LINE2EVAL} --training_text passportline2strings_eval.txt

# extract a starting point to start lstm training from
combine_tessdata -e ../tessdata_best/eng.traineddata trainfrom.lstm
# this is going to take a while, test different parameters to see how they converge
./train_different_network_parameters_line1.sh
./train_different_network_parameters_line2.sh
# different network parameters didn't make a significant difference

# create graphs and store them in a pdf
Rscript make_line1_graphs.R
Rscript make_line2_graphs.R

# create trainingdata file for tesseract to use
lstmtraining --stop_training --continue_from test_line1/output_num_GRID_1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1/lstm-out/base_checkpoint --traineddata line1train/eng/eng.traineddata --model_output ./passportline1.traineddata
lstmtraining --stop_training --continue_from test_line2/output_num_GRID_1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1/lstm-out/base_checkpoint --traineddata line2train/eng/eng.traineddata --model_output ./passportline2.traineddata

# test individual images like this
# tesseract ./passportimages/1.png ./test --tessdata-dir ./ --oem 1 --psm 6 -l passportline1+passportline2
# cat test.txt