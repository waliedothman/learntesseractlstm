#!/bin/bash

# examples for reference https://github.com/Shreeshrii/tessdata_shreetest
# tesseract 4.1 would allow to whitelist chars using the lstm engine, tesseract 4 does not

BIRTHDATETRAIN=birthdatetrain
BIRTHDATEEVAL=birthdateeval

mkdir ${BIRTHDATETRAIN}
mkdir ${BIRTHDATEEVAL}


# generate synthetic data, if only for a test run, you might want to change the number of generated strings to a single digit
python3 ./machinereadablepassport/generatedriverlicensestrings.py

# use --training_text to feed it to tesstrain.sh
tesstrain.sh --fonts_dir ./fonts --lang ind --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "ektp" --output_dir ${BIRTHDATETRAIN} --training_text drivers_license_train.txt --distort_image 
tesstrain.sh --fonts_dir ./fonts --lang ind --linedata_only --noextract_font_properties --langdata_dir ./langdata --tessdata_dir ./tessdata_best --fontlist "ektp" --output_dir ${BIRTHDATEEVAL} --training_text drivers_license_test.txt --distort_image 

# exit 0

# extract a starting point to start lstm training from
combine_tessdata -e ./tessdata_best/ind.traineddata trainfrom_driver_license.lstm
# this is going to take a while, test different parameters to see how they converge
./train_different_network_parameters_driver_license.sh
# different network parameters didn't make a significant difference

# create graphs and store them in a pdf
Rscript make_driver_license_graphs.R

# create trainingdata file for tesseract to use
lstmtraining --stop_training --continue_from test_driver_license/output_num_GRID_1.36.0.1_Ct3.3.16_Mp3.3_Lfys64_Lfx96_Lrx96_Lfx256_O1c1/lstm-out/base_checkpoint --traineddata birthdatetrain/ind/ind.traineddata --model_output ./driverlicensebirthdate.traineddata

# test individual images like this
# tesseract ./passportimages/1.png ./test --tessdata-dir ./ --oem 1 --psm 6 -l passportline1+passportline2
# cat test.txt