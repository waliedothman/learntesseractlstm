#!/bin/bash
declare -a net_spec_1=("36" "48") # (1,36,0,1) or (1,48,0,1)
declare -a net_spec_2_3=("3") # Ct3,3,16 and Mp3,3 
declare -a net_spec_4=("Lfys48" "Lfys64")
declare -a net_spec_5=("Lfx96")
declare -a net_spec_6=("Lrx96")
declare -a net_spec_7=("Lfx128" "Lfx192" "Lfx256" "Lfx384" "Lfx512")
declare -a net_spec_8=("O1c1")

mkdir test_NIK
for param_1 in "${net_spec_1[@]}"
do
   for param_2_3 in "${net_spec_2_3[@]}"
   do
      for param_4 in "${net_spec_4[@]}"
         do
            for param_5 in "${net_spec_5[@]}"
            do
               for param_6 in "${net_spec_6[@]}"
               do
                  for param_7 in "${net_spec_7[@]}"
                  do
                     for param_8 in "${net_spec_8[@]}"
                     do
                        
			param_insert_1A="1.${param_1}.0.1"
			param_insert_1B="1,${param_1},0,1"
			param_insert_2A="Ct${param_2_3}.${param_2_3}.16"
			param_insert_2B="Ct${param_2_3},${param_2_3},16"
			param_insert_3A="Mp${param_2_3}.${param_2_3}"
			param_insert_3B="Mp${param_2_3},${param_2_3}"
			
			link_line="/test_NIK/output_num_GRID_${param_insert_1A}_${param_insert_2A}_${param_insert_3A}_${param_4}_${param_5}_${param_6}_${param_7}_${param_8}"
			
			mkdir .${link_line}
			mkdir .${link_line}/lstm-out
			# rm .${link_line}/lstm-out/*
			
			lstmtraining --debug_interval -1 \
			   --traineddata NIKtrain/eng/eng.traineddata \
			   --net_spec "[${param_insert_1B} ${param_insert_2B} ${param_insert_3B} ${param_4} ${param_5} ${param_6} ${param_7} O1c111]" \
			   --model_output .${link_line}/lstm-out/base \
                --continue_from ./trainfrom_NIK.lstm \
                --old_traineddata ./tessdata_best/eng.traineddata \
			   --train_listfile NIKtrain/eng.training_files.txt \
			   --eval_listfile NIKeval/eng.training_files.txt \
			   --max_iterations 5000 &>.${link_line}/basetrain.log


                        
done
done
done
done
done
done
done
